<?php
namespace Nitra\GeoBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Doctrine\Common\Inflector\Inflector;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class NitraGeoExtension extends Extension implements PrependExtensionInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // получить настройки
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
        
        // создать параметры
        $container->setParameter('nitra_geo.ds_url', $config['ds_url']);
        $container->setParameter('nitra_geo.ds_token', $config['ds_token']);
    }
    
    /**
     * {@inheritDoc}
     */
    public function prepend(ContainerBuilder $container)
    {
        // Изменить настройки doctrine ORM
        $this->prependORM($container);
    }
    
    /**
     * Изменить настройки doctrine ORM
     */
    protected function prependORM(ContainerBuilder $container)
    {
        
        // если не установлена doctrine
        if (!$container->hasExtension('doctrine')) {
            // прервать выполнение
            return;
        }
        
        // получить конфигурацию бандла
        $extensionConfig = $container->getExtensionConfig('nitra_geo');
        // провеверить если в config.yml НЕ добавлены настройки
        // прерываем выполнение
        if (!isset($extensionConfig[0]['entity'])) {
            return;
        }
        
        // массив интерфейсов реализующих сушности
        $resolveTargetEntities = array(
            'orm' => array(
                'resolve_target_entities' => array()
        ));
        
        // обойти все сушности банлда
        foreach($extensionConfig[0]['entity'] as $modelName => $modelClass) {
            // интерфейс сущности
            $interfaceName = 'Nitra\\GeoBundle\\Entity\\Model\\' . Inflector::classify($modelName) . 'Interface';
            $resolveTargetEntities['orm']['resolve_target_entities'][$interfaceName] = $modelClass;
            
            // создать параметр сущности
            $parameterName = 'nitra_geo.entity.'.$modelName;
            $container->setParameter($parameterName, $modelClass);
        }
        
        // создать параметры интерфейсов реализующих сушности
        $container->setParameter('nitra_geo.resolve_target_entities', $resolveTargetEntities['orm']['resolve_target_entities']);
        
        // добавить массив интерефейсов в настройки doctrine
        $container->prependExtensionConfig('doctrine', $resolveTargetEntities);
    }
    
    
}
