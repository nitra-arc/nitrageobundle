<?php

namespace Nitra\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\GeoBundle\Entity\Region
 * @ORM\MappedSuperclass()
 */
class Region implements Model\RegionInterface
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    
    /**
     * @var integer $businessKey
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $businessKey;
    
    /**
     * @var string $name
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(min="3", max="255")
     */
    protected $name;

    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
    
    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set businessKey
     * @param integer $businessKey
     * @return Region
     */
    public function setBusinessKey($businessKey)
    {
        $this->businessKey = $businessKey;
    
        return $this;
    }

    /**
     * Get businessKey
     * @return integer 
     */
    public function getBusinessKey()
    {
        return $this->businessKey;
    }

    /**
     * Set name
     * @param string $name
     * @return Region
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    
}