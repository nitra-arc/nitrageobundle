<?php

namespace Nitra\GeoBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('nitra_geo')->addDefaultsIfNotSet();
        
        // добавить настройки 
        $this->addApiUrl($rootNode);
        
        // добавить настройки сущностей
        $this->addEntity($rootNode);
        
        // вернуть дерево настроек
        return $treeBuilder;
    }
    
    
    /**
     * Добавить настройки ссылка в API DeliverySync для тетрадки
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addApiUrl(ArrayNodeDefinition $treeBuilder)
    {
        
        $treeBuilder
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('ds_url')
                    ->defaultValue('http://ds3.nitralabs.com/sync/')
                ->end()
                // {token} - ds3.nitralabs.com - Nitra\DeliveryBundle\Entity\Client::$token
                ->scalarNode('ds_token')
                    ->isRequired()
                    ->cannotBeEmpty()
                ->end()
            ->end();
    }
    
    /**
     * добавить настройки сущностей
     * @param ArrayNodeDefinition $treeBuilder
     */
    protected function addEntity(ArrayNodeDefinition $treeBuilder)
    {
        // установить конфигурационные данные
        $treeBuilder->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('entity')->isRequired()->cannotBeEmpty()
                ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('city')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('region')->isRequired()->cannotBeEmpty()->end()
                    ->end()
                ->end()
            ->end();
    }
    
}
