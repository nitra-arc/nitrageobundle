<?php

namespace Nitra\GeoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Knp\DoctrineBehaviors\Model as ORMBehaviors;

/**
 * Nitra\GeoBundle\Entity\City
 * @ORM\MappedSuperclass()
 */
class City implements Model\CityInterface
{
    
    use ORMBehaviors\Timestampable\Timestampable,
        ORMBehaviors\Blameable\Blameable,
        ORMBehaviors\SoftDeletable\SoftDeletable;
    
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var integer $businessKey
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $businessKey;
    
    /**
     * @ORM\ManyToOne(targetEntity="Nitra\GeoBundle\Entity\Model\RegionInterface")
     * @ORM\JoinColumn(nullable=false)
     * @Assert\NotBlank(message="Не указан регион")
     */
    protected $region;
    
    /**
     * @var string $name
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min="3", max="255")
     * @Assert\NotBlank(message="Не указано название")
     */
    protected $name;
    
    /**
     * this object to string
     * @return string
     */
    public function __toString()
    {
        return (string)$this->name;
    }
    
    /**
     * Get id
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set businessKey
     * @param integer $businessKey
     * @return City
     */
    public function setBusinessKey($businessKey)
    {
        $this->businessKey = $businessKey;
    
        return $this;
    }

    /**
     * Get businessKey
     * @return integer 
     */
    public function getBusinessKey()
    {
        return $this->businessKey;
    }

    /**
     * Set name
     * @param string $name
     * @return City
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set region
     * @param \Nitra\GeoBundle\Entity\Model\RegionInterface $region
     * @return City
     */
    public function setRegion(\Nitra\GeoBundle\Entity\Model\RegionInterface $region)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     * @return \Nitra\GeoBundle\Entity\Model\RegionInterface
     */
    public function getRegion()
    {
        return $this->region;
    }
    
}