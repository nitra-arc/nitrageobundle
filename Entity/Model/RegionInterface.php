<?php

namespace Nitra\GeoBundle\Entity\Model;

/**
 * RegionInterface
 */
interface RegionInterface
{
    
    /**
     * this object to string
     * @return string
     */
    public function __toString();
    
    /**
     * Get businessKey
     * @return integer 
     */
    public function getBusinessKey();

    /**
     * Get name
     * @return string 
     */
    public function getName();
    
}
