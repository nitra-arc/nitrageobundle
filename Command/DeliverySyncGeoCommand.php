<?php

namespace Nitra\GeoBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\GeoBundle\Command\DeliverySync;

/**
 * DeliverySyncGeoCommand
 * Синхронизация географии 
 */
class DeliverySyncGeoCommand extends DeliverySync
{
    
    /**
     * Настройка команды
     */
    protected function configure()
    {
        // настройка команды
        $this
            ->setName('nitra:geo:sync')
            ->setDescription('Синхронизировать географию.')
            ->setHelp(<<<EOT
Синхронизировать географию.
Сервис синхронизации:  http://ds3.nitralabs.com
GitHub: https://github.com/nitra/DeliverySync
EOT
                );
        ;
    }
    
    /**
     * Получить class реализующий город
     * @return string class город
     */
    public function getCityClass()
    {
        return $this->getContainer()->getParameter('nitra_geo.entity.city');
    }
    
    /**
     * Получить class реализующий регион
     * @return string class регион 
     */
    public function getRegionClass()
    {
        return $this->getContainer()->getParameter('nitra_geo.entity.region');
    }
    
    /**
     * {@inheritDoc}
     * @throw Exception - ошибка выполнения синхронизации
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        // отправить xml запрос на сервер
        $apiResponse = $this->apiSendRequest('syncronizeGeo');
        
        // проверить ответ
        if (!$apiResponse instanceof \stdClass) {
            $errorMessage = date('Y-m-d H:i'). " - Ответ не был получен от сервера.";
            throw new \Exception($errorMessage);
        }
        
        // проверить тип ответа
        if (!isset($apiResponse->type) || !$apiResponse->type) {
            $errorMessage = date('Y-m-d H:i'). " - Не указан тип ответа сервера.";
            throw new \Exception($errorMessage);
        }
        
        // ошибка синхронизации
        if ($apiResponse->type == 'error') {
            $errorMessage = date('Y-m-d H:i'). " - Ошибка синхронизации.";
            $errorMessage.= (isset($apiResponse->message) && $apiResponse->message) ? " " .$apiResponse->message : '';
            throw new \Exception($errorMessage);
        }
        
        // проверить ответ должен содердать регионы
        if (!isset($apiResponse->regions) || !$apiResponse->regions) {
            $errorMessage = date('Y-m-d H:i'). " - Ответ сервера не содержит ни одного региона.";
            throw new \Exception($errorMessage);
        }
        
        // проверить ответ должен содердать города
        if (!isset($apiResponse->cities) || !$apiResponse->cities) {
            $errorMessage = date('Y-m-d H:i'). " - Ответ сервера не содержит ни одного города.";
            throw new \Exception($errorMessage);
        }
        
        // выполнить синхронизацию регионов
        $this->processSyncRegions($apiResponse->regions, $output);
        
        // выполнить синхронизацию городов
        $this->processSyncCities($apiResponse->cities, $output);
    }
    
    /**
     * получить все регионы тетрадки
     * array( businessKey => name )
     * @return array
     */
    protected function getTetradkaRegions()
    {
        
        // гидратор KeyPair в отдельном бандле отсутсвует
        // получаем через объектную модель
        $regions = $this->getEntityManager()
            ->getRepository($this->getRegionClass())
            ->createQueryBuilder('r')
            ->where('r.businessKey IS NOT NULL')
            ->getQuery()
            ->getArrayResult();
        
        // преобразовать к виду array( businessKey => name )
        $result = array();
        foreach($regions as $region) {
            $result[$region['businessKey']] = $region['name'];
        }
        
        // вернуть массив
        return $result;
    }
    
    /**
     * получить все регионы тетрадки
     * array( businessKey => id)
     * @return array
     */
    protected function getTetradkaRegionIds()
    {
        
        // гидратор KeyPair в отдельном бандле отсутсвует
        // получаем через объектную модель
        $regions = $this->getEntityManager()
            ->getRepository($this->getRegionClass())
            ->createQueryBuilder('r')
            ->where('r.businessKey IS NOT NULL')
            ->getQuery()
            ->getArrayResult();
        
        // преобразовать к виду array( businessKey => name )
        $result = array();
        foreach($regions as $region) {
            $result[$region['businessKey']] = $region['id'];
        }
        
        // вернуть массив
        return $result;
    }
    
    
    /**
     * получить все города тетрадки
     * @return array
     */
    protected function getTetradkaCities()
    {
        
        // гидратор KeyPair в отдельном бандле отсутсвует
        // получаем через объектную модель
        $cities = $this->getEntityManager()
            ->getRepository($this->getCityClass())
            ->createQueryBuilder('c')
            ->where('c.businessKey IS NOT NULL')
            ->getQuery()
            ->getArrayResult();
        
        // преобразовать к виду array( businessKey => name )
        $result = array();
        foreach($cities as $city) {
            $result[$city['businessKey']] = $city['name'];
        }
        
        // вернуть массив 
        return $result;
    }
    

    /**
     * выполнить синхронизацию регионов
     * @param stdClass $dsRegions - массив регионов DeliverySync
     * @param OutputInterface $output
     */
    protected function processSyncRegions(\stdClass $dsRegions, OutputInterface $output)
    {
        
        // получить прогресс
        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, count((array)$dsRegions));
        
        // получить все регионы тетрадки
        $tetradkaRegions = $this->getTetradkaRegions();
        
        // обойти массив регионов
        foreach($dsRegions as $dsRegion) {
            
            // проверить существует ли регион в регионах тетрадки
            $businessKey = $dsRegion->id;
            if (in_array($businessKey, array_keys($tetradkaRegions))) {
                // удаляем из массива регионов 
                // оставшиеся регионы в массиве будут удалены
                unset($tetradkaRegions[$businessKey]);
                
            } else {
                // в тетрадке нет региона
                // добавить новый регион
                $entityClass = $this->getRegionClass();
                $region = new $entityClass;
                $region->setName($dsRegion->name);
                $region->setBusinessKey($businessKey);
                
                // запомнить для сохранения
                $this->getEntityManager()->persist($region);
            }
            
            // обновить прогресс
            $progress->advance();
        }
        
        // регионы не пришли в синхронизации 
        if ($tetradkaRegions) {
            // получить удаляемые регионы
            // если удаляем через createQueryBuilder()->delete()
            // то не срабатывет SoftDeletable, запись удаялется физически из БД
            $regionsDelete = $this->getEntityManager()
                ->getRepository($this->getRegionClass())
                ->createQueryBuilder('region')
                ->where('region.businessKey IN(:ids)')->setParameter('ids', array_keys($tetradkaRegions))
                ->getQuery()
                ->execute();
            // удалить города которые не пришли в синхронизации 
            foreach($regionsDelete as $region) {
                $this->getEntityManager()->remove($region);
            }
        }
        
        // сохранить филиалы отдельно от городов, если таковые имеются
        $this->getEntityManager()->flush();
        
        // Синхронизация завершена
        $output->write(' ');
        $output->write('Синхронизация регионов завершена успешно.');
        // завершить прогресс
        $progress->finish();
    }
    
    /**
     * выполнить синхронизацию регионов
     * @param stdClass $dsCities - массив городов DeliverySync
     * @param OutputInterface $output
     */
    protected function processSyncCities(\stdClass $dsCities, OutputInterface $output)
    {
        
        // получить прогресс
        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, count((array)$dsCities));
        
        // получить массив регионов тетрадки
        $tetradkaRegionIds = $this->getTetradkaRegionIds();
        
        // получить массив горолов тетрадки
        // array( businessKey => name )
        $tetradkaCities = $this->getTetradkaCities();
        
        // обойти массив городов
        foreach($dsCities as $dsCity) {
            
            // проверить существует ли город в городах тетрадки
            $businessKey = $dsCity->id;
            if (in_array($businessKey, array_keys($tetradkaCities))) {
                // удаляем из массива городов
                // оставшиеся города в массиве будут удалены
                unset($tetradkaCities[$businessKey]);
                
            } else {
                // в тетрадке нет города
                // добавить новый город 
                
                $entityClass = $this->getCityClass();
                $city = new $entityClass();
                $city->setName($dsCity->name);
                $city->setBusinessKey($businessKey);
                $city->setRegion($this->getEntityManager()->getReference($this->getRegionClass(), $tetradkaRegionIds[$dsCity->regionId]));
                
                // запомнить для сохранения
                $this->getEntityManager()->persist($city);
            }
            
            // обновить прогресс
            $progress->advance();
        }
        
        // города не пришли в синхронизации 
        if ($tetradkaCities) {
            // получить удаляемые города
            // если удаляем через createQueryBuilder()->delete()
            // то не срабатывет SoftDeletable, запись удаялется физически из БД
            $citiesDelete = $this->getEntityManager()
                ->getRepository($this->getCityClass())
                ->createQueryBuilder('city')
                ->where('city.businessKey IN(:ids)')->setParameter('ids', array_keys($tetradkaCities))
                ->getQuery()
                ->execute();
            // удалить города которые не пришли в синхронизации 
            foreach($citiesDelete as $city) {
                $this->getEntityManager()->remove($city);
            }
        }
        
        // сохранить города  отдельно от филиалов, если таковые имеются
        $this->getEntityManager()->flush();
        
        // Синхронизация завершена
        $output->write(' ');
        $output->write('Синхронизация городов завершена успешно.');
        // завершить прогресс
        $progress->finish();
    }
    
}