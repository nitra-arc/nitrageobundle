<?php

namespace Nitra\GeoBundle\Entity\Model;

/**
 * Description of CityInterface
 */
interface CityInterface
{
    
    /**
     * Get id
     * @return id $id
     */
    public function getId();
    
    /**
     * this object to string
     * @return string
     */
    public function __toString();
    
    /**
     * Get businessKey
     * @return integer 
     */
    public function getBusinessKey();

    /**
     * Get name
     * @return string 
     */
    public function getName();

    /**
     * Get region
     * @return \Nitra\GeoBundle\Entity\Model\RegionInterface
     */
    public function getRegion();
    
}
