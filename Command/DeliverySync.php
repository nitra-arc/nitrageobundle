<?php

namespace Nitra\GeoBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * DeliverySync
 * Общий класс синхронизации 
 * даныных из API DeliverySync
 */
abstract class DeliverySync extends ContainerAwareCommand
{
    
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $em;
    
    /**
     * получить EntityManager
     * @return \Doctrine\ORM\EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }
    
    /**
     * {@inheritDoc}
     * выполнить подготовку перед выполенением
     * @thow /Exception если команда не полчает все необходимые данные для выполенения
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // установить EntityManager
        $this->em = $this->getContainer()->get('doctrine')->getManager();
    }
    
    /**
     * Отправить запрос на сервер
     * @param string $command - название команды синхронизации
     * @param array  $options - массив параметров передаваемых на сервер DeliverySync
     * @return stdClass $apiResponse - ответ сервера
     */
    protected function apiSendRequest($command, array $options=null)
    {
        
        // команда синхронизации отправляемая на сервер
        $sendParams = array(
            'command' => $command,
            'options' => $options,
        );
        
        // ссылка API DS
        $apiUrl = $this->getContainer()->getParameter('nitra_geo.ds_url')
            . $this->getContainer()->getParameter('nitra_geo.ds_token');
        
//        // ссыка команды передаваемая в ds.nitralabs.com
//        print "\n"; print_r($sendParams); print "\n";
//        echo $apiUrl.'?'.http_build_query($sendParams)."\n";
//        die;
        
        // отправить запрос на сервер 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($sendParams));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $apiResponse = curl_exec($ch);
        curl_close($ch);
        
        // вернуть ответ сервера
        return json_decode($apiResponse);
    }
    
}
