# NitraGeoBundle

## Описание
Данный банлд предназначен для синхронизации географии 
с сайта http://ds3.nitralabs.com


## Подключение
Для подключения данного модуля в проект необходимо:


* composer.json:

```json
{
    ...   
    "require": {
        ...
        "nitra/geobundle": "dev-master",
        "nitra/doctrine-behaviors": "dev-master",
        ...
    }
    ...
}
```

* app/AppKernel.php:

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\GeoBundle\NitraGeoBundle(),
            new Nitra\ExtensionsAdminBundle\NitraExtensionsAdminBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

* app/config/routing.yml:

```yaml
#...
NitraGeoBundle:
    resource: "@NitraGeoBundle/Resources/config/routing.yml"
    prefix:   /
#...
```


## Конфигурация по умолчанию:
```yaml
    # app/config/config.yml

    # ...
    nitra_geo:      
      # API DeliverySync https://github.com/nitra/DeliverySync
      # ссылка в API DeliverySync для тетрадки
      # http://ссылка_на_сервис_api/{token} - токен авторизции в API
      # {token} - ds3.nitralabs.com - Nitra\DeliveryBundle\Entity\Client::$token
      api_url:
    # ...
```


## Конфигурация
### Настройка
Конфигурация вынесена в файл parameters.yml в app/config проекта.

```yaml
    # app/config/parameters.yml

    nitra_geo_api_url: http://ds3.nitralabs.com/{token}

```


## Комманды
php app/console nitra-geo:sync-geo
Синхронизировать географию.
Сервис синхронизации:  http://ds3.nitralabs.com
GitHub: https://github.com/nitra/DeliverySync
