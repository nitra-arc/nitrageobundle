<?php

namespace Nitra\GeoBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadCityData
 */
class LoadCityData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var Symfony\Component\DependencyInjection\Container
     */
    protected $container;
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 1;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // сущность 
        $entityClass = $this->container->getParameter('nitra_geo.entity.city');
        
        // создаем город
        $city1 = new $entityClass();
        $city1->setName('GeoCity1');
        $city1->setRegion($this->getReference('GeoRegion1'));
        $this->setReference('GeoCity1', $city1);
        $manager->persist($city1);
        
        // создаем город
        $city2 = new $entityClass();
        $city2->setName('GeoCity2');
        $city2->setRegion($this->getReference('GeoRegion2'));
        $this->setReference('GeoCity2', $city2);
        $manager->persist($city2);
        
        // создаем город
        $city3 = new $entityClass();
        $city3->setName('GeoCity3');
        $city3->setRegion($this->getReference('GeoRegion3'));
        $this->setReference('GeoCity3', $city3);
        $manager->persist($city3);
        
        // сохранить
        $manager->flush();
    }
    
}
