<?php

namespace Nitra\GeoBundle\Command;

use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Nitra\GeoBundle\Command\DeliverySync;

/**
 * DeliverySyncWarehouseCommand
 * Синхронизация складов
 */
class DeliverySyncWarehouseCommand extends DeliverySync
{
    
    /**
     * @var object ТК 
     * формируется из $input->getOption('bk')
     */
    protected $delivery;
    
    /**
     * @var string $deliveryClass
     * класс реализующий ТК
     */
    protected $deliveryClass = 'Nitra\\MainBundle\\Entity\\Delivery';
    
    /**
     * @var string $warehouseClass
     * класс реализующий склад
     */
    protected $warehouseClass = 'Nitra\\MainBundle\\Entity\\Warehouse';
    
    /**
     * Настройка команды
     */
    protected function configure()
    {
        // настройка команды
        $this
            ->setName('nitra:geo:sync-warehouses')
            ->setDescription('Синхронизировать склады.')
            ->addOption('bk', null, InputOption::VALUE_REQUIRED, 'BusinessKey транспортной компании (ID ТК в DS), для которой будут синхронизироваться склады.')
            ->setHelp(<<<EOT
Синхронизировать склады для транспортной компании.
<info>php app/console nitra:geo:sync-warehouses --bk="integer"</info>
--bk - BusinessKey транспортной компании (ID ТК в DS).
Сервис синхронизации:  http://ds3.nitralabs.com
GitHub: https://github.com/nitra/DeliverySync
EOT
                );
        ;
    }
    
    /**
     * {@inheritDoc}
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        // выполнить инициализацию родителем
        parent::initialize($input, $output);
        
        // Получить ТК 
        $delivery = $this->getEntityManager()
            ->getRepository($this->getDeliveryClass())
            ->findOneByBusinessKey($input->getOption('bk'));
        
        // проверить ТК
        if (!$delivery) {
            throw new \Exception('Не найдена ТК по указанному параметру BusinessKey: '.$input->getOption('bk'));
        }
        
        // установить ТК
        $this->setDelivery($delivery);
    }
    
    /**
     * получить $deliveryClass class реализующий ТК
     * @return string 
     */
    public function getDeliveryClass()
    {
        return $this->deliveryClass;
    }
    
    /**
     * установить $deliveryClass class реализующий ТК
     * @param string $class - имя класса 
     */
    public function setDeliveryClass($class)
    {
        $this->deliveryClass = $class;
    }
    
    /**
     * получить $warehouseClass class реализующий склад
     * @return string 
     */
    public function getWarehouseClass()
    {
        return $this->warehouseClass;
    }
    
    /**
     * установить $warehouseClass class реализующий склад
     * @param string $class - имя класса 
     */
    public function setWarehouseClass($class)
    {
        $this->warehouseClass = $class;
    }
    
    /**
     * Получить class реализующий город
     * @return string class город
     */
    public function getCityClass()
    {
        return $this->getContainer()->getParameter('nitra_geo.entity.city');
    }
    
    /**
     * получить ТК
     * @return object
     */
    public function getDelivery()
    {
        return $this->delivery;
    }
    
    /**
     * установить ТК
     * @param object $delivery
     */
    public function setDelivery($delivery)
    {
        // проверить принадлежность ТК к склассу 
        $deliveryClass = $this->getDeliveryClass();
        if (!$delivery instanceof $deliveryClass) {
            throw new \Exception('ТК должна быть реализована классом "' . $deliveryClass . '".');
        }
        // установить ТК
        $this->delivery = $delivery;
    }
    
    
    /**
     * Выполнить команду
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throw Exception - ошибка выполнения синхронизации
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        
        // отправить xml запрос на сервер
        $apiResponse = $this->apiSendRequest('syncronizeWarehouses', array(
            'deliveryId' => $this->getDelivery()->getBusinessKey(),
        ));
        
        // проверить ответ
        if (!$apiResponse instanceof \stdClass) {
            $errorMessage = date('Y-m-d H:i'). " - Ответ не был получен от сервера.";
            throw new \Exception($errorMessage);
        }
        
        // проверить тип ответа
        if (!isset($apiResponse->type) || !$apiResponse->type) {
            $errorMessage = date('Y-m-d H:i'). " - Не указан тип ответа сервера.";
            throw new \Exception($errorMessage);
        }
        
        // ошибка синхронизации
        if ($apiResponse->type == 'error') {
            $errorMessage = date('Y-m-d H:i'). " - Ошибка синхронизации.";
            $errorMessage.= (isset($apiResponse->message) && $apiResponse->message) ? " " .$apiResponse->message : '';
            throw new \Exception($errorMessage);
        }
        
        // проверить ответ должен содердать склады
        if (!isset($apiResponse->warehouses) || !$apiResponse->warehouses) {
            $errorMessage = date('Y-m-d H:i'). " - Ответ сервера не содержит ни одного склада.";
            throw new \Exception($errorMessage);
        }
        
        // выполнить синхронизацию городов
        $this->processSyncWarehouses($apiResponse->warehouses, $output);
    }
    
    /**
     * выполнить синхронизацию складов
     * @param stdClass $dsWarehouses - массив складов DeliverySync
     * @param OutputInterface $output
     */
    protected function processSyncWarehouses(\stdClass $dsWarehouses, OutputInterface $output)
    {
        
        // получить прогресс
        $progress = $this->getHelperSet()->get('progress');
        $progress->start($output, count((array)$dsWarehouses));
        
        // получить города Тетрадки 
        // ключ массива ID города на стороне DS
        $tetradkCities = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('city.id, city.businessKey, city.name')
            ->from($this->getCityClass(), 'city', 'city.businessKey')
            ->getQuery()
            ->getArrayResult();
        
        // получить склады тетрадки по синхронизируемой ТК 
        $tetradkaWarehouses = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('w.id, w.businessKey, w.address')
            ->from($this->getWarehouseClass(), 'w', 'w.businessKey')
            ->where('w.delivery = :delivery')->setParameter('delivery', $this->getDelivery())
            ->getQuery()
            ->getArrayResult();
        
        // обойти массив ответа
        foreach($dsWarehouses as $dsWarehouse) {
            
            // проверить существует ли склад в складах тетрадки 
            $businessKey = $dsWarehouse->id;
            if (!isset($tetradkaWarehouses[$businessKey])) {
                // склад в тетрадке не существует
                // создать новый склад
                $warehouseClass = $this->getWarehouseClass();
                $warehouse = new $warehouseClass();
                $warehouse->setDelivery($this->getDelivery());
                $warehouse->setBusinessKey($businessKey);
                
                // запомнить для сохранения
                $this->getEntityManager()->persist($warehouse);
                
            } else {
                // склад в тетрадке сушествует 
                // получить склад
                $warehouse = $this->getEntityManager()->getReference($this->getWarehouseClass(), $tetradkaWarehouses[$businessKey]['id']);
                
                // удаляем из массива складов 
                // оставшиеся склады в массиве будут удалены
                unset($tetradkaWarehouses[$businessKey]);
            }
            
            // массив сравнения склада DS 
            $dsWhCompare = serialize(array(
               (string)$dsWarehouse->id,
               (string)$dsWarehouse->deliveryId,
               (string)$dsWarehouse->cityId,
               (string)$dsWarehouse->address,
            ));
            
            // массив сравнения склада тетрадки
            $tetradkaWhCompare = serialize(array(
                (string)$warehouse->getBusinessKey(),
                (string)(($warehouse->getDelivery()) ? $warehouse->getDelivery()->getBusinessKey() : null),
                (string)(($warehouse->getCity()) ? $warehouse->getCity()->getBusinessKey() : null),
                (string)$warehouse->getAddress(),
            ));
            
            // сравнить склад DS и склад Тетрадки
            if ($dsWhCompare != $tetradkaWhCompare) {
                
                // идентификатор города на стороне DS
                $businessKey = $dsWarehouse->cityId;
                // установить город склада
                if (isset($tetradkCities[$businessKey])) {
                    $warehouse->setCity($this->getEntityManager()->getReference($this->getCityClass(), $tetradkCities[$businessKey]['id']));
                }
                
                // наполнить склад Тетрадки данными
                $warehouse->setAddress($dsWarehouse->address);
            }
            
            // обновить прогресс
            $progress->advance();
        }
        
        // склады не пришли в синхронизации
        // ТК не работает с оставшимися складами
        if ($tetradkaWarehouses) {
            // получить удаляемые склады для ТК
            // если удаляем через createQueryBuilder()->delete()
            // то не срабатывет SoftDeletable, запись удаялется физически из БД
            $warehousesDelete = $this->getEntityManager()
                ->getRepository($this->getWarehouseClass())
                ->createQueryBuilder('w')
                ->where('w.businessKey IN(:ids)')->setParameter('ids', array_keys($tetradkaWarehouses))
                ->andWhere('w.delivery = :delivery')->setParameter('delivery', $this->getDelivery())
                ->getQuery()
                ->execute();
            // удалить склады которые не пришли в синхронизации 
            foreach($warehousesDelete as $wh) {
                $this->getEntityManager()->remove($wh);
            }
        }
        
        // сохранить склады
        $this->getEntityManager()->flush();
        
        // Синхронизация завершена
        $output->write(' ');
        $output->write('Синхронизация складов для ТК "' . $this->getDelivery() . '" завершена успешно.');
        // завершить прогресс
        $progress->finish();
    }
    
    
}
