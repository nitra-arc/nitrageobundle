<?php

namespace Nitra\GeoBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * LoadRegionData
 */
class LoadRegionData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    
    /**
     * @var Symfony\Component\DependencyInjection\Container
     */
    protected $container;
    
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 0;
    }
    
    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        // уствноыить контейнер
        $this->container = $container;
    }
    
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // сущность 
        $entityClass = $this->container->getParameter('nitra_geo.entity.region');
        
        // создаем регион
        $region1 = new $entityClass();
        $region1->setName('GeoRegion1');
        $this->setReference('GeoRegion1', $region1);
        $manager->persist($region1);
        
        // создаем регион
        $region2 = new $entityClass();
        $region2->setName('GeoRegion2');
        $this->setReference('GeoRegion2', $region2);
        $manager->persist($region2);
        
        // создаем регион
        $region3 = new $entityClass();
        $region3->setName('GeoRegion3');
        $this->setReference('GeoRegion3', $region3);
        $manager->persist($region3);
        
        // сохранить
        $manager->flush();
    }
    
    
}

